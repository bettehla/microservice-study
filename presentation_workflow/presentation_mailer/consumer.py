import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("buldvasdvvadfvadfvadfvadfv")
    message = json.loads(body)
    send_mail(
        'Your presentation has been approved',
        message["presenter_name"]
        +
        "your presentation "
        +
        message["title"]
        +
        " has been approved",
        "admin@conference.go",
        [message['presenter_email']],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    # print("Received %r" % body)
    message = json.loads(body)
    send_mail(
        'Your presentation has been rejected',
        message["presenter_name"]
        +
        " unfortunately, your presentation "
        +
        message["title"]
        +
        " has been rejected",
        "admin@conference.go",
        [message['presenter_email']],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
